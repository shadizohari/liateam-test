// https://shopapi.rc.liadev.ir/api/rest/v1/get_product?categories=10&page=2
// https://shopapi.rc.liadev.ir/api/rest/v1/get_categories
import { Provider } from 'react-redux';
import { configureStore } from './store/configureStore';
import React from 'react';
import './assets/font-fa/font.css';
import './assets/sass/main.scss';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import Categories from './pages/Categories';
import Products from './pages/Products';
import Header from './components/Header';
import Footer from './components/Footer'
function App() {
  let store = configureStore();

  return (
    <div className="App">
      <Provider store={store}>
        <Router>
          <Header />
          <Switch>
            <Route path="/" component={Categories} exact />
            <Route path="/:product/:id" component={Products} exact />
          </Switch>
          <Footer />
        </Router>
      </Provider>
    </div>
  );
}

export default App;

