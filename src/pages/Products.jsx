import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { addCart } from '../store/actions/addCart';
import Title from '../components/Title';
import { useParams } from "react-router-dom";
import { Grid, Container, Typography, CardMedia, CardContent, Card, CardActions } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { formatPrice } from '../utils/utils'
import { FaCartPlus } from 'react-icons/fa';
import spinner from "../assets/img/Spinner.gif";
import Footer from '../components/Footer';

const useStyles = makeStyles((theme) => ({
    titleCard: {
        margin: 0,
        marginBottom: "5px",
        fontSize: "16px",
        '&.css-anmq6p-MuiTypography-root': {
            marginTop: "20px",
            marginBottom: "0"

        },
        textAlign: "center",
    },
    volume: {
        textAlign: "center",
        color: "#ec407a",
        marginTop: 0
    },
    newLabel: {
        width: "40%",
        textAlign: "center",
        border: "1px solid #ec407a",
        borderRadius: "50px",
        color: "#ec407a",
        lineHeight: "24px",
        marginRight: "10px"

    },
    flex: {
        displayL: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    cartPlus: {
        color: "#0090ff",
        fontSize: "32px",
        marginRight: "10px",
        cursor: "pointer"
    },
    price: {
        fontSize: "24px",
        fontWeight: 700,
        border: "1px solid transparent",
        paddingLeft: "20px",
        paddingRight: "50px"

    },
    toman: {
        color: "#d8d8d8",
        fontSize: "15px"
    },
    card: {
        '& .css-anmq6p-MuiTypography-root': {
            transition: ".1s"
        },
        '&:hover': {
            '&.css-1ri6ub7-MuiPaper-root-MuiCard-root': {
                boxShadow: "0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)",
            },
            '& .css-anmq6p-MuiTypography-root': {
                color: "#fff !important",
                background: "#ec407a",
                border: "1px solid #ec407a",
                borderRadius: "0px 50px 50px 0px"
            },
        },
    },
}));
export default function Products() {
    const classes = useStyles();
    const { product, id } = useParams();
    const [products, setProducts] = useState();
    const dispatch = useDispatch();

    useEffect(() => {
        fetch(`http://localhost:8080/https://shopapi.rc.liadev.ir/api/rest/v1/get_product?categories=${id}`)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(response.error);
                }
                return response.json();
            }).then(data => {
                setProducts({ ...data })
            })
            .catch(error => console.error(error))
    }, [products])

    function addToCart(item) {
        if (item.price) {
            dispatch(addCart(item));
        }
    }

    return (
        <>
            <Title title={product} />
            <Container maxWidth="lg">
                <Grid container spacing={3} >
                    {((products?.list.length > 0) ? products.list.map((item, index) => {
                        return (
                            <Grid item xs={12} sm={6} md={3} key={item.id} style={{ display: "flex", alignItems: "stretch" }}>
                                <Card sx={{ maxWidth: 345 }} className={classes.card}>
                                    <CardMedia
                                        style={{ cursor: "pointer" }}
                                        component="img"
                                        image={`https://shopapi.rc.liadev.ir/${products.list[index].medium_pic}`}
                                        alt={`${item.title}`}
                                    />
                                    <CardContent style={{ padding: 0 }}>
                                        <p className={classes.newLabel}>
                                            جدید
                                        </p>
                                        <p className={classes.titleCard}>
                                            {`${item.title}`}
                                        </p>
                                        <p className={classes.volume}>
                                            {`${item.volume}`}
                                        </p>
                                        <CardActions className={classes.flex} style={{ padding: 0, paddingBottom: "15px" }}>

                                            <FaCartPlus className={`${classes.cartPlus}`} onClick={() => addToCart(item)} />
                                            <Typography gutterBottom variant="p" component="div" className={classes.price}>
                                                {(item?.price ? `${formatPrice(item.price.final_price)} ` : "ناموجود")}
                                                {(item?.price && <span className={classes.toman}>تومان</span>)}
                                            </Typography>
                                        </CardActions>
                                    </CardContent>
                                </Card>
                            </Grid>
                        )
                    })
                   
                    : <div className="spinner"><img src={spinner} alt="spinner" /></div>)}
                </Grid>
            </Container >
        </>
    )
}
