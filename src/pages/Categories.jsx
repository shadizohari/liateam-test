import React, { useEffect, useState } from 'react'
import Container from '@mui/material/Container';
import Title from '../components/Title';
import { Link} from 'react-router-dom';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import { CardActionArea } from '@mui/material';
import spinner from "../assets/img/Spinner.gif";
export default function Categories() {
    const [categories, setCategories] = useState([])
    useEffect(() => {
        fetch('http://localhost:8080/https://shopapi.rc.liadev.ir/api/rest/v1/get_categories')
            .then((response) => {
                if (!response.ok) {
                    console.error(response);
                    throw new Error(response.error);
                }
                return response.json();
            }).then(data => {
                setCategories([...data])
            })
            .catch(error => console.error(error))
    }, [])
    return (
        <>
            <Title title="دسته‌بندی" />
            <Container maxWidth="lg">
                <Grid container spacing={10}>

                    {((categories?.length > 0) ? categories.map((item, index) => {
                        return (
                            <Grid item xs={12} md={6} key={item.id}>
                                <Link to={`${item.name}/${item.id}`}>
                                    <Card>
                                        <CardActionArea>
                                            <CardMedia
                                                component="img"
                                                image={`https://shopapi.rc.liadev.ir/${categories[index].image}`}
                                                alt={`${categories[index].name}`}
                                            />
                                        </CardActionArea>
                                    </Card>
                                </Link>
                            </Grid>
                        )
                    }) : <div className="spinner"><img src={spinner} alt="spinner"/></div>)}
                </Grid>
            </Container >
        </>
    )
}
