import { ActionTypes } from "../actionTypes/actionTypes.js";

const initialState = { allCount: 0 };


function shoppingCartReduser(state = initialState, action) {
    switch (action.type) {
        case ActionTypes.ADD_CART:
            if (!state[action.payload.id]) {
                state[action.payload.id] = { item: action.payload, count: 1 }
            } else {
                state[action.payload.id].count++
            }
            state.allCount++;
            return { ...state };
        case ActionTypes.DELETE_CART:
            state.allCount = state.allCount - state[action.payload.id].count;
            delete state[action.payload.id]
            return { ...state };
        case ActionTypes.REDUSE_CART:
            if (state[action.payload.id] && state[action.payload.id].count > 1) {
                state.allCount--;
                state[action.payload.id].count--;
                return { ...state };
            } else if (state[action.payload.id] && state[action.payload.id].count === 1) {
                state.allCount--;
                delete state[action.payload.id];
                return { ...state };
            }
            break;
            default:
            return state;
    }
}

export default shoppingCartReduser;