import { combineReducers } from 'redux';
import shoppingCartReduser from './shoppingCartReduser';


const RootReducer = combineReducers({
    shoppingCart: shoppingCartReduser,
})

export default RootReducer;