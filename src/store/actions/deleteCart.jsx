import  {ActionTypes}  from "../actionTypes/actionTypes.js";


export const deleteCart = (item) => {
    return {
      type: ActionTypes.DELETE_CART,
      payload: item,
    };
  };