import  {ActionTypes}  from "../actionTypes/actionTypes.js";


export const addCart = (item) => {
    return {
      type: ActionTypes.ADD_CART,
      payload: item,
    };
  };