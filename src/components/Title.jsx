import React from 'react'
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';


export default function Title({title,...props}) {
    return (
        <>
            <Container maxWidth="lg">
                <Grid container>
                    <h1 style={{ marginTop: "40px", marginBottom: "40px", fontWeight: "lighter" }}>{title}</h1>
                </Grid>

            </Container>
        </>
    )
}
