import React, { useState, useEffect } from 'react'
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import logo from '../assets/img/logo.png';
import { MdKeyboardArrowDown } from 'react-icons/md';
import { FaShoppingCart } from 'react-icons/fa';
import profile from "../assets/img/db8188e44e1e2932531cc4a2afed1e36";
import { useDispatch, useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import { Card, CardContent, CardMedia, Typography, Box, TextField, Button } from '@mui/material/';
import { formatPrice } from '../utils/utils'
import { AiOutlineClose } from 'react-icons/ai';
import { makeStyles } from '@mui/styles';
import { deleteCart } from '../store/actions/deleteCart';
import { addCart } from '../store/actions/addCart';
import { reduseCart } from '../store/actions/reduseCart';

const useStyles = makeStyles((theme) => ({
    cardDark: {
        '&.css-1oqv2c0-MuiPaper-root-MuiCard-root': {
            background: "#252629",
            color: "#d2d2d2"
        }
    },
    cardLight: {
        '&.css-1oqv2c0-MuiPaper-root-MuiCard-root': {
            background: "#2d2d30",
            color: "#d2d2d2"
        }
    },
    box: {
        display: 'flex',
        flexDirection: 'column',
        width: "100%"
    },
    input: {
        '& .css-9ddj71-MuiInputBase-root-MuiOutlinedInput-root': {
            color: "#d2d2d2",
            textAlign: "center"
        },
        '& .css-1d3z3hw-MuiOutlinedInput-notchedOutline': {
            borderColor: "#d2d2d2",
            borderColor: "transparent",
        },
        '& .css-1t8l2tu-MuiInputBase-input-MuiOutlinedInput-input': {
            width: "20px",
            textAlign: "center",
            padding: "5px"
        }
    },
    btnCart: {
        '&.css-1e6y48t-MuiButtonBase-root-MuiButton-root': {
            color: "#d2d2d2",
            minWidth: "20px"
        }
    }
}));

export default function Header() {
    const classes = useStyles();
    const cart = useSelector((store) => store.shoppingCart);
    const dispatch = useDispatch();
    const [visibilityBadge, setvisibilityBadge] = useState()
    const [cartNumber, setCartNumber] = useState()
    const [isOpen, setIsOpen] = useState("displayNone")
    function toggle() {
        if (cart.allCount > 0) {
            setIsOpen((isOpen) => {
                if (isOpen === "displayNone") {
                    return "display"
                } else {
                    return "displayNone"
                }
            })
        } else {
            setIsOpen("displayNone")
        }
    }
    const [total, setTotal] = useState(0)

    useEffect(() => {
        if (cart.allCount > 0) {
            let sum = 0
            setvisibilityBadge("visibilityBadge")
            setCartNumber(cart.allCount)
            Object.keys(cart).forEach((item) => {
                if (item !== "allCount") {
                    sum = sum + (cart[item].count * (cart[item].item.price.final_price))
                }
            })
            setTotal(sum)
        } else {
            setTotal(0)
            setvisibilityBadge("")
            setIsOpen("displayNone")
        }

    }, [cart])

    function deleteItem(item) {
        dispatch(deleteCart(item))
    }

    return (
        <div className="parent-header">
            <Container maxWidth="xl" className="container-fluid">
                <Grid container>
                    <Grid item xs={9}>
                        <Grid className="flex" >
                            <img className="header-logo" src={logo} alt="logo" />
                            <Link to="/" className="header-link">خانه</Link>
                            <Link to="/" className="header-link">فروشگاه</Link>
                            <Link to="/" className="header-link">وبلاگ</Link>
                            <Link to="/" className="header-link">درباره ما</Link>
                            <Link to="/" className="header-link">تماس با ما</Link>

                        </Grid>
                    </Grid>
                    <Grid item xs={3} className="flex justify-space">
                        <div className="flex">
                            <span>رضا پورجباری عزیز</span>
                        </div>
                        <div className="flex">
                            <div className="img-parernt">
                                <img src={profile} alt="profile" className="img-profile double-border" />
                            </div>

                            <MdKeyboardArrowDown className="header-arrow" />
                        </div>
                    </Grid>
                </Grid>
                <hr className="hr" />
                <Grid container style={{ height: "66px", alignItems: "center" }}>
                    <Grid item xs={9}>
                        <Grid className="flex" >
                            <Link to="" className="nav-link">مراقبت پوست</Link>
                            <Link to="" className="nav-link">مراقبت مو</Link>
                            <Link to="" className="nav-link">مراقبت بدن</Link>
                            <Link to="" className="nav-link">آرایشی</Link>
                            <Link to="" className="nav-link">پرفروش‌ترین</Link>
                            <Link to="" className="nav-link">جدیدترین</Link>
                        </Grid>
                    </Grid>
                    <Grid item xs={3} className="nav-shopping">
                        <div className="parent-shopping-icon">
                            <FaShoppingCart className="heder-shopping-icon" onClick={toggle} />
                            <div className={`cart-badge ${visibilityBadge}`}>{cartNumber}</div>
                            <div className={`dropdown-content ${isOpen}`}>
                                {Object.keys(cart)?.map((keyName, i) => {

                                    if (keyName !== "allCount") {

                                        return (
                                            <Card key={keyName} className={((i + 1) % 2) ? `${classes.cardDark}` : `${classes.cardLight}`} sx={{ display: 'flex' }}>
                                                <CardMedia
                                                    component="img"
                                                    sx={{ width: 151 }}
                                                    image={`https://shopapi.rc.liadev.ir/${cart[keyName].item.medium_pic}`}
                                                    alt="Live from space album cover"
                                                />
                                                <Box className={classes.box}>
                                                    <CardContent >
                                                        <Typography component="div" variant="p">
                                                            {`${cart[keyName].item.title}`}
                                                        </Typography>
                                                        <Typography variant="subtitle1" component="div" className="priceShopping">
                                                            {`${formatPrice(cart[keyName].item.price.final_price)} تومان`}
                                                        </Typography>
                                                        <div className="closeCard">
                                                            <AiOutlineClose style={{ cursor: "pointer" }} onClick={() => deleteItem(cart[keyName].item)} />
                                                        </div>
                                                        <div className="btns-cart">
                                                            
                                                            <Button className={classes.btnCart} onClick={()=> dispatch(addCart(cart[keyName].item))} variant="text">+</Button>
                                                            <TextField
                                                                value={`${cart[keyName].count}`}
                                                                className={classes.input}
                                                            />
                                                            <Button className={classes.btnCart} variant="text" onClick={()=>dispatch(reduseCart(cart[keyName].item))}>-</Button>
                                                        </div>
                                                    </CardContent>

                                                </Box>
                                            </Card>
                                        )
                                    }
                                })}
                                <Card className={((Object.keys(cart).length) % 2) ? `${classes.cardDark}` : `${classes.cardLight}`} sx={{ display: 'flex' }}>
                                    <CardContent style={{ width: "100%" }}>
                                        <div className="totalPrice">
                                            <p>جمع کل:</p>
                                            <p>{`${formatPrice(total)} تومان`}</p>
                                        </div>
                                    </CardContent>
                                </Card>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </Container>
            <hr className="hr-shadow" />
        </div >
    )
}
