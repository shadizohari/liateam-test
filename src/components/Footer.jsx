import React from 'react'
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import { FaPhoneVolume } from 'react-icons/fa';
import { MdOutlineMailOutline } from 'react-icons/md';
import logo from '../assets/img/4b64a1969b8dcac7372fab851736328c';
import { FaInstagram, FaFacebookF, FaTwitter, FaYoutube } from 'react-icons/fa'
export default function Footer() {
    return (
        <div className="parent-footer">
            <Container maxWidth="xl" className="container-fluid borderBottomFooter">
                <Grid container style={{ height: "75px" }}>
                    <Grid container xs={12} md={6} style={{ justifyContent: "space-between", alignItems: "center" }}>
                        <div>
                            مرکز پشتیلانی بازاریابان
                        </div>
                        <div className="vertical-border"></div>
                        <div>021-88888888<FaPhoneVolume className="icon-phone" /></div>
                        <div className="vertical-border"></div>
                        <div style={{ display: "flex", alignItems: "center" }}>sellersupport@liateam.com<MdOutlineMailOutline className="icon-email" /></div>

                    </Grid>
                    <Grid container xs={12} md={6} style={{ display: "flex", flexDirection: "row-reverse", alignItems: "center" }}>
                        <img src={logo} className="logo-footer" />
                        <h1 style={{ fontWeight: "lighter", marginLeft: "50px" }}>Lia Virtual Office</h1>
                        <h3 style={{ fontWeight: "lighter", marginLeft: "50px" }}>Good Times Good News</h3>
                    </Grid>
                </Grid>

            </Container>
            <Container maxWidth="xl" className="container-fluid">
                <Grid container style={{ height: "75px" }}>
                    <Grid container xs={6} md={4} className="p-footer" style={{ alignItems: "center" }}>
                        <p>همه روزها و ساعت های اداری پاسخگوی شما هستیم.</p>
                    </Grid>
                    <Grid container xs={6} md={4} style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                        <div className="icon-social">
                            <FaYoutube />
                        </div>
                        <div className="icon-social">
                            <FaTwitter />
                        </div>
                        <div className="icon-social">
                            <FaFacebookF />
                        </div>
                        <div className="icon-social">
                            <FaInstagram />
                        </div>
                    </Grid>
                    <Grid container xs={6} md={4} style={{ alignItems: "center" }}>
                        <p className="p-footer">© تمام حقوق این وب سایت متعلق به شرکت آرمان تدبیر اطلس 1400-1398 می باشد.</p>
                    </Grid>
                </Grid>

            </Container>
        </div>
    )
}
