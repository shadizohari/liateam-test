
# LiaTeam Sample Project

This is a small React.js project to demonstrate some frontend development skills.


## Getting Started

1. Clone this project
2. Open a terminal in the cloned directory
3. Run: `npm install`
4. Run: `npm start`
5. Open a browser and go to: http://localhost:3000

In order to resolve CORS issues with the API's used for the project,
a simple NodeJS proxy server is used. This proxy server is based on the open-source
`CORS Anywhere` project hosted on GitHub. This server is setup and started as below:

1. Clone the project from: https://github.com/Rob--W/cors-anywhere
2. Open a terminal in the cloned directory
3. Run: `npm install`
4. Run: `node server.js`


## Getting in Touch

If you like my work you can contact me via email: `shadi`.`zohari` at Gmail.
